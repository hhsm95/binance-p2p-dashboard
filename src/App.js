import React, { useState, useEffect } from 'react';
import dateFormat from "dateformat";
import { Line } from 'react-chartjs-2';


const App = () => {
  const [rows, setRows] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const now = new Date();
      now.setHours(now.getHours() - 1);

      const date = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
      fetch(`https://consultaunica.mx/apis/binance-p2p?from-date=${date}`)
        .then(resp => resp.json())
        .then(data => {
          if (data.success) {
            setRows(data.data);
          }
        });
    };

    const id = setInterval(() => {
      fetchData();
    }, 60 * 1000);

    fetchData();

    return () => clearInterval(id);

  }, []);

  const usdt = rows.filter(row => row.coin === 'USDT');
  const busd = rows.filter(row => row.coin === 'BUSD');

  const labels = [];
  const values1 = [];
  const values2 = [];

  usdt.forEach(row => {
    const time = row.created_date.substring(11, 16);
    labels.push(time);
    values1.push(row.price);
  });

  busd.forEach(row => {
    values2.push(row.price);
  });


  const data = {
    labels,
    datasets: [
      {
        label: 'USDT/MXN',
        data: values1,
        fill: false,
        backgroundColor: 'rgb(16, 185, 129)',
        borderColor: 'rgba(16, 185, 129, 0.2)',
      },
      {
        label: 'BUSD/MXN',
        data: values2,
        fill: false,
        backgroundColor: 'rgb(245, 158, 11)',
        borderColor: 'rgba(245, 158, 11, 0.2)',
      },
    ],
  };

  const options = {
    responsive: true,
    interaction: {
      mode: 'index',
      intersect: false,
    },
    stacked: false,
    scales: {
      y: {
        type: 'linear',
        display: true,
        position: 'right',
        ticks: {
          callback: function (value, index, values) {
            return '$' + value.toFixed(2);
          }
        }
      }
    }
  };

  return (
    <div style={{
      maxWidth: "1200px",
      margin: "auto",
      padding: "16px 32px"
    }}>
      <h3 style={{ textAlign: "center" }}>Shows the lowest price to buy USDT from Binance P2P in MXN</h3>
      <Line data={data} options={options} />
      <ul>
        <li>
          <a href="https://www.linkedin.com/in/hhsm95/" rel="noreferrer" target="_blank">🤓 My LinkedIn</a>
        </li>
        <li>
          <a href="https://hhsm95.dev/" rel="noreferrer" target="_blank">📖 My Blog</a>
        </li>
        <li>
          <a href="https://gitlab.com/hhsm95/binance-p2p-dashboard" rel="noreferrer" target="_blank">👨‍💻 Dashboard source in GitLab</a>
        </li>
        <li>
          <a href="https://gitlab.com/hhsm95/binance-p2p-api" rel="noreferrer" target="_blank">👨‍💻 Backend source in GitLab</a>
        </li>
      </ul>
    </div>
  );
}

export default App;
